# Feature flags -- introduction

SPIIR is moving towards a 'feature-flag' model of development.

In this model, work is done on feature branches, but these feature branches are
regularly merged back in to the main development branch.  This is true even if
the work in the feature branch is not fully complete, working, or tested
(although it mustn't break the build or imperil the stability of other parts of
SPIIR).

Instead, incomplete features are hidden behind 'feature flags' that disable
them fully when the user doesn't explicitly opt into them when running the
pipeline. Note that for the model to be effective, it is important that all
aspects of the feature are hidden behind its flag.

The purpose of this document is to (a) document some SPIIR conventions for
using feature flags, (b) list all current feature flags, and (c) serve as a
historical record of previous flags.

# SPIIR feature flag conventions

In general, developers should feel free to add new feature-flag--protected
features. There is no desire to encourage a bureaucracy-heavy approach to this.

Every feature should be protected by a boolean command line option in this form:

    --feature-xyz

The name `xyz` should be short, descriptive, and not clash with (or be a prefix
of) any other feature name. You can use a multi-part name (e.g.
`signal-removal`).

Ideally, the entire feature should be protected by this flag: running the
pipeline without the flag should be unchanged.

Additional feature-specific flags and parameters should be prefixed with
`feature-xyz-`. For example, tuning parameters for a `flooble` feature might be
called:

    --feature-flooble-alpha=123.456
    --feature-flooble-beta=1.2e-4
    --feature-flooble-use-wobbly

Generally speaking, feature flags are for hiding features that are not yet
stable or well-tested enough to be run in production.

Ultimately, it's a goal that the version of the pipeline in production has no
`--feature` flags enabled. In practice, given the timelines for reviews makes
this difficult, we don't regard it as a mistake or failure if the final
production configuration has a feature flag enabled. Similarly, in rare cases
a critical bug might be identified in a feature while the pipeline is running
with that feature in production: in such cases it might make more sense to
turn the flag off rather than produce, and review, a new pipeline version.

Nevertheless, it's crucially important that the final proposed production
configuration is reviewed and tested thoroughly.

When the feature is considered stable, has been thoroughly tested, and has
completed an LSC review, the `feature` nomenclature should be dropped and the
parameters may be renamed appropriately.


# SPIIR current feature flags

## signal-removal

*Name*: Signal removal

*Feature flag*: `--feature-signal-removal-bg`

*Parameters*: (see `--help` for full details)
 * --feature-signal-removal-bg-threshold

*Short description*: Improve FARs by removing signal-like backgrounds from the
background statistics calculations.

*Relevant merge requests*: lscsoft/spiir!189

*Relevant review slides*:

*Production plan*: Aim to review this during O4a. When the feature flag is
removed, it will be on all the time but can be effectively disabled by settng a
very high threshold.

*History*:
 * (as of 2023-06-27) currently in branch `signal_removal_bg`

*Other notes*:

## best-far

*Name*: Best FAR

*Feature flag*: `--feature-best-far`

*Parameters*: (see `--help` for full details)
 * --feature-best-far-threshold

*Short description*: Improve FARs by selecting the best (lowest) FAR rather 
than the worst (highest) FAR from the three timescales.

*Relevant merge requests*: lscsoft/spiir!195

*Relevant review slides*: 

*Production plan*: Aim to review this during O4a. When the feature flag is 
removed, it will become the default behaviour. It may be tuned by setting
different background collection timescales or a different threshold.

*History*:
 * (as of 2023-09-19) currently in branch `use_best_far`

*Other notes*:
The threshold may be dependent on detector sensitivity.

## dump-stats

*Name*: Dump marginalized stats

*Feature flag*: `--feature-dump-stats`

*Parameters*: (see `--help` for full details)
 * `--feature-dump-stats-dir-path`

*Short description*: Enable to dump marginalized stats files to uniquely timestamped directories, for use in testing.
The parameter `--feature-dump-stats-dir-path` sets the base directory to record marginalized stats in timestamped subdirectories.
This will be used in combination with an upcoming feature to read these files during an injection run, using the most up to date
stats available.

*Relevant merge requests*: 

*Relevant review slides*: 

*Production plan*: This flag will be used for offline tests, likely in combination with `--feature-subprocesses-can-block`, for
determinism. It should allow a more accurate recreation of online results in following injection tests, as the stats will, in effect,
be updated live.

*History*:
 * (202x-xx-xx) In branch tdv-subprocesses-can-block

*Other notes*:


## cluster-available-triggers

*Name*: Cluster available triggers

*Feature flag*: `--feature-cluster-available-triggers`

*Parameters*: (see `--help` for full details)
 * None

*Short description*: Enable to cluster upload candidates only in the second 
they are received, saving at least a second of latency.
Without this flag, SPIIR will wait to receive an additional second of data,
and will only upload a trigger if it finds no better trigger in that data.

*Relevant merge requests*: https://git.ligo.org/lscsoft/spiir/-/merge_requests/172

*Relevant review slides*:

*Production plan*: Aim to review this during O4a. When the feature flag is 
removed, it will be on all the time.

*History*:
 * (as of 2023-10-09) currently in branch `tdavies__cluster_best_each_second`

*Other notes*:

## rescale-chisq-dof

*Name*: Rescale Chisq Degrees Of Freedom

*Feature flag*: `--feature-rescale-chisq-dof`

*Parameters*: (see `--help` for full details)
 * None

*Short description*: Improve reduced chisq calculation by rescaling the chisq 
degrees of freedom. Without this feature, chisq dof may be overestimated.

*Relevant merge requests*: lscsoft/spiir!205

*Relevant review slides*: 

*Production plan*: Aim to review this during O4a. The new method is still being
considered. If correct, once reviewed, the feature flag may be removed and the
feature will be enabled permanently.

*History*:
 * (as of 2023-10-30) currently in branch `rescale_chisq_dof`

*Other notes*: 

## subprocesses-can-block

*Name*: Subprocesses can block

*Feature flag*: `--feature-subprocesses-can-block`

*Parameters*: (see `--help` for full details)
 * None

*Short description*: Enable to allowing some subprocesses to block to improve determinism at the cost of performance.
Without this feature enabled, the pipeline may use outdated marginalized stats (background tables)
while waiting for updated marginalized stats to be processed. Additionally, without this feature
enabled, new subprocesses to update marginalized stats will not be launched until the old ones
are completed, which leads to inconsistent stats timestamps.

*Relevant merge requests*: 

*Relevant review slides*: 

*Production plan*: This flag should be enabled only in offline tests where FAR determinism is required,
or when marginalized stats should be procduced at fixed times. In online runs, whatever stats are 
currently available should be used, as performance is a higher concern. If review is required, this flag
may be replaced with a normal command line option, or a config file for offline tests.


*History*:
 * (202x-xx-xx) In branch tdv-subprocesses-can-block

*Other notes*:

## weight-cmbchisq

*Name*: Weight combined chisq

*Feature flag*: `--feature-weight-cmbchisq`

*Parameters*: (see `--help` for full details)
 * None

*Short description*: Weight the combined chisq by summing each detector's
reduced chisq, weighted by its contribution to coherent snr.
e.g. `cmbchisq = (snr_H1^2 * chisq_H1 + snr_L1^2 * chisq_L1) / coherent_snr`

*Relevant merge requests*: lscsoft/spiir!200

*Relevant review slides*: 

*Production plan*: Aim to review this during O4a. The new method is still being
considered. If correct, once reviewed, the feature flag may be removed and the
feature will be enabled permanently.

*History*:
 * (as of 2023-10-30) Currently in branch `tdavies__weight_cmbchisq`

*Other notes*: 

## dump-all-zerolags

*Name*: Dump zerolags before clustering

*Feature flag*: `--feature-dump-all-zerolags`

*Parameters*: (see `--help` for full details)
 * None

*Short description*: Enable to record all zerolags received by finalsink, rather than just zerolags which survive clustering. Each file is dumped in snapshots alongside the usual zerolags file. The new filenames follow the old format, with an additional `_all` suffix. e.g. `013_zerolag_1186642720_7982_all.xml.gz`

*Relevant merge requests*: 

*Relevant review slides*: 

*Production plan*: This flag may be enabled on background test runs, but will otherwise become an optional debug flag.

*History*:

# SPIIR past feature flags

None so far.


# Template for future feature flags
Copy, paste and replace!

## feature-flag-name

*Name*: Short, human-comprehensible name

*Feature flag*: `--feature-xyz`

*Parameters*: (see `--help` for full details)
 * list all parameters for this feature flag here

*Short description*: A short description of the scientific or computing impact of
this feature flag. Include a link to a paper/technical note/set of slides if
able

*Relevant merge requests*: list all relevant merge requests here

*Relevant review slides*: list links to all review slides here

*Production plan*: A short description of the plan for how this

*History*:
 * (202x-xx-xx) Merged work-in-progress in to `spiir-O4-EW-development`
 * (202x-yy-yy) Now feature complete (see MR 123)
 * (202x-zz-zz) Fixed bugs (see MR 456)
 * (202x-tt-tt) Subject of review call: http://wiki.ligo.org/...

*Other notes*: Anything else of relevance
